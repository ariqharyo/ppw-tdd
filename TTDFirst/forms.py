from django.forms import ModelForm
from .models import CommentFieldModel

class CommentFieldForm(ModelForm):
    class Meta:
        model = CommentFieldModel
        fields = ['comment']

    # def __init__(self, *args, **kwargs):
    #     self.entry = kwargs.pop('entry')   # the blog entry instance
    #     super().__init__(*args, **kwargs)

    # def save(self):
    #     comment = super().save(commit=False)
    #     comment.entry = self.entry
    #     comment.save()
    #     return comment
