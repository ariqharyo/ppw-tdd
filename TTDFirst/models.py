from django.db import models



# Create your models here.
 
class CommentFieldModel(models.Model):
    comment = models.TextField(max_length = 300)
    timePosted = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.comment
    