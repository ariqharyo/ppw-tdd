from django.contrib import admin
from .models import CommentFieldModel

# Register your models here.

admin.site.register(CommentFieldModel)
