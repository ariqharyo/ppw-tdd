from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
from django.test import TestCase, Client
from django.urls import resolve
from .forms import *
from .models import *
from .views import *
import time


# Create your tests here.
class TTDDFirstUnitTest(TestCase):
    def test_url_main_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_profile_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)
    
    def test_views_using_main_func(self):
        found = resolve('/')
        self.assertEqual(found.func, main)
    
    def test_views_using_CommentFieldView_func(self):
        found = resolve('/form/')
        self.assertEqual(found.func, CommentFieldView)
    
    def test_views_CommentFieldView_response_get_check(self):
        found = self.client.get('/form/')
        self.assertRedirects(found, expected_url='/', status_code=302, target_status_code=200)
    
    def test_model_save_form(self):
        test ="Ariq Haryo Setiaki"
        jumlah_sebelumnya = CommentFieldModel.objects.filter(comment=test).count()
        response = Client().post('/form/', {"comment": test})
        self.assertEqual(CommentFieldModel.objects.filter(comment=test).count(), jumlah_sebelumnya+1)

    def test_model_comment_max_character(self):
        test = "a"*301
        response = Client().post('/form/', {"comment": test})
        self.assertEqual(CommentFieldModel.objects.filter(comment=test).count(), 0)
    
    def test_model_print_str_comment(self):
        string = "Ariq"
        model = CommentFieldModel(comment=string)
        self.assertEqual(str(model), string)

    def test_profile_contains_name(self):
        response = Client().get('/profile/')
        html_response = response.content.decode('utf-8')
        self.assertIn("Ariq Haryo Setiaki", html_response)
    
    def test_profile_contains_npm(self):
        response = Client().get('/profile/')
        html_response = response.content.decode('utf-8')
        self.assertIn('1706043853', html_response)
    
    def test_profile_contains_kuliah(self):
        response = Client().get('/profile/')
        html_response = response.content.decode('utf-8')
        self.assertIn("Universitas Indonesia", html_response)
    
    def test_profile_contains_hobi(self):
        response = Client().get('/profile/')
        html_response = response.content.decode('utf-8')
        self.assertIn("Bermain catur", html_response)

    def test_profile_contains_deskripsi(self):
        response = Client().get('/profile/')
        html_response = response.content.decode('utf-8')
        self.assertIn("Saya tampan", html_response)

class SeleniumTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver.exe')
        self.browser.implicitly_wait(20)
        super(SeleniumTest, self).setUp()

    def tearDown(self):
        self.browser.implicitly_wait(10)
        self.browser.quit()
    
    def test_can_submit_comment(self):
        self.browser.get('http://shareyourmind.herokuapp.com/')
        comment = self.browser.find_element_by_id("id_comment")
        submit = self.browser.find_element_by_css_selector("button[type='submit']")
        comment.send_keys('Coba coba')
        submit.send_keys(Keys.RETURN)
        time.sleep(5)
        self.assertIn("Coba coba", self.browser.page_source)
    
    def test_is_page_tittle_Hello_Apa_Kabar(self):
        self.browser.get('http://shareyourmind.herokuapp.com/')
        pageTittle = self.browser.find_element_by_css_selector("div.d-flex.align-items-center h6").text
        time.sleep(5)
        self.assertEqual(pageTittle, "Hello, Apa kabar?")

    def test_is_button_text_Save_Person(self):
        self.browser.get('http://shareyourmind.herokuapp.com/')
        buttonText = self.browser.find_element_by_css_selector("button.btn.btn-success").text
        time.sleep(5)
        self.assertEqual(buttonText, "Save person")

    def test_is_button_using_styling_btn_btn_success(self):
        self.browser.get('http://shareyourmind.herokuapp.com/')
        buttonStylingClass = self.browser.find_element_by_css_selector("button[type='submit']").get_attribute("class")
        time.sleep(5)
        self.assertEqual(buttonStylingClass, "btn btn-success")
    
    def test_is_navigation_bar_usign_nav_pills_nav_fill(self):
        self.browser.get('http://shareyourmind.herokuapp.com/')
        navigationStylingClass = self.browser.find_element_by_css_selector("div.navigation nav").get_attribute("class")
        time.sleep(5)
        self.assertEqual(navigationStylingClass, "nav nav-pills nav-fill")




        

        


