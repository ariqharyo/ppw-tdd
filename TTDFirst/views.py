from django.shortcuts import render, redirect
from .forms import CommentFieldForm
from .models import CommentFieldModel

# Create your views here.

mhs_name = 'Ariq Haryo Setiaki'
npm = 1706043853
kuliah = "Universitas Indonesia"
hobi = "Bermain catur"
deskripsi = "Saya tampan"

def main(request):
    comment_data = CommentFieldModel.objects.all()
    response = {'form' : CommentFieldForm(), 'comment_data' : comment_data}
    return render(request, 'form.html', response)

def CommentFieldView(request):
    if(request.method == 'POST'):
        form = CommentFieldForm(request.POST)
        if(form.is_valid()):
            model = CommentFieldModel(
                comment = form.cleaned_data['comment']
            )
            model.save()
        else:
            form = CommentFieldForm()
        return redirect('/')
    elif(request.method == 'GET'):
        return redirect('/')

def profile(request):
    response = {'name': mhs_name, 'kuliah': kuliah, 'npm': npm,
                'hobi' : hobi, 'deskripsi' : deskripsi}
                
    return render(request, 'index_lab1.html', response)
